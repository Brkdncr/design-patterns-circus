﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern5
{
    public enum LessonType
    {
        Article = 1,
        Video = 2
    }
    public abstract class LessonBuilder
    {
        public Lesson _Lesson { get; set; }
        protected LessonType _LessonType;
        protected LessonBuilder(LessonType LessonType_)
        {
            _LessonType = LessonType_;
        }
        public abstract void GenerateLessonType();
        public abstract void GenerateUrl();
    }
}
