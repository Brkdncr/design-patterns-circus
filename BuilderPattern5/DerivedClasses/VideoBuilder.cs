﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern5
{
    public class VideoBuilder : LessonBuilder
    {
        public VideoBuilder(Lesson Lesson_)
            : base(LessonType.Video)
        {
            base._Lesson = Lesson_;
        }
        public override void GenerateLessonType()
        {
            throw new NotImplementedException();
        }

        public override void GenerateUrl()
        {
            throw new NotImplementedException();
        }
    }
}
