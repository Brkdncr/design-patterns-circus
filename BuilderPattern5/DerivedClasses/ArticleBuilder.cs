﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern5
{
    public class ArticleBuilder : LessonBuilder
    {
        public ArticleBuilder(Lesson Lesson_)
            : base(LessonType.Article)
        {
            base._Lesson = Lesson_;
        }
        public override void GenerateLessonType()
        {
            base._LessonType = LessonType.Article;
        }

        public override void GenerateUrl()
        {
            base._Lesson.Url = String.Format("Makale.aspx?id={0}", base._Lesson.ID);
        }
    }
}
