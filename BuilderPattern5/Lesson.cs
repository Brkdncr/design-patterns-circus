﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern5
{
    public class Lesson
    {
        public int ID { get; set; }
        public string Header { get; set; }
        public string Url { get; set; }


    }
}
