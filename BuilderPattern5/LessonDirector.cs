﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern5
{
    public class LessonDirector //Director
    {
        private LessonBuilder _lessonBuilder;
        public void GenerateLesson(LessonBuilder LessonBuilder_)
        {
            _lessonBuilder = LessonBuilder_;
            _lessonBuilder.GenerateLessonType();
            _lessonBuilder.GenerateUrl();
        }
        public void ShowLesson()
        {
            Console.WriteLine(_lessonBuilder._Lesson);
        }
    }
}
