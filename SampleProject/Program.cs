﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleProject
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath_ = @"D:\gsktest.xml";

            string replacedVal_ = File.ReadAllText(filePath_);
            replacedVal_ = replacedVal_.Replace("<?xml version=\"UTF-8\"?>", "");
            File.WriteAllText(filePath_, replacedVal_);
            DataSet ds_ = new DataSet("MASTER");
            ds_.ReadXml(filePath_);

        }
    }
}
