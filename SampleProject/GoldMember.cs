﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleProject
{
    public class GoldMember : Member
    {
        private string _username;
        private string _password;

        public GoldMember(string Username_, string Password_)
            : base(Username_, Password_)
        {

        }
        public override string UserName
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
            }
        }

        public override string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }

        public override Member GetMember()
        {
            throw new NotImplementedException();
        }
    }
}
