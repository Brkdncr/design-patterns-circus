﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleProject
{
    public abstract class Member
    {
        public abstract string UserName { get; set; }

        public abstract string Password { get; set; }

        public Member(string Username_, string Password_)
        {
            this.UserName = Username_;
            this.Password = Password_;
        }

        public abstract Member GetMember();

    }
}
