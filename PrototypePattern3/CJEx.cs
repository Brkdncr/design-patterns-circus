﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern3
{
    public class CJEx : AProtagonistEx
    {
        public override AProtagonistEx Clone()
        {
            CJEx cloned_ = (CJEx)this.MemberwiseClone();
            cloned_.Details = new AdditionalDetails();
            cloned_.Details.Charisma = this.Details.Charisma;
            cloned_.Details.Fitness = this.Details.Fitness;
            return cloned_ as AProtagonistEx;
        }
    }
}
