﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern3
{
    public abstract class AProtagonist
    {
        private int m_health;

        public int Health
        {
            get { return m_health; }
            set { m_health = value; }
        }

        private int m_felony;

        public int Felony
        {
            get { return m_felony; }
            set { m_felony = value; }
        }

        private double m_money;

        public double Money
        {
            get { return m_money; }
            set { m_money = value; }
        }

        public abstract AProtagonist Clone();
    }
}
