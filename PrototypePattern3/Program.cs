﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern3
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Clone - Copy

            CJ player = new CJ();
            player.Health = 1;
            player.Felony = 10;
            player.Money = 2.0;

            Console.WriteLine("Original Player stats:");
            Console.WriteLine("Health: {0}, Felony: {1}, Money: {2}",
                player.Health.ToString(),
                player.Felony.ToString(),
                player.Money.ToString());

            // player object that we can save on the disk asyncronously.
            CJ playerToSave = player.Clone() as CJ;

            Console.WriteLine("\nCopy of player to save on disk:");
            Console.WriteLine("Health: {0}, Felony: {1}, Money: {2}",
                playerToSave.Health.ToString(),
                playerToSave.Felony.ToString(),
                playerToSave.Money.ToString());

            #endregion

            #region Shallow Copy - Detail

            // The code to demonstrate the shallow copy
            CJEx playerEx = new CJEx();
            playerEx.Health = 1;
            playerEx.Felony = 10;
            playerEx.Money = 2.0;
            playerEx.Details.Fitness = 5;
            playerEx.Details.Charisma = 5;

            CJEx shallowClonedPlayer = playerEx.Clone() as CJEx;
            shallowClonedPlayer.Details.Charisma = 10;
            shallowClonedPlayer.Details.Fitness = 10;

            #endregion

            #region CJFinal - ICloneable

            // Let us see how we can perform the deep copy now
            CJFinal player = new CJFinal();
            player.Health = 1;
            player.Felony = 10;
            player.Money = 2.0;
            player.Details.Fitness = 5;
            player.Details.Charisma = 5;

            // lets clone the object but this time perform a deep copy
            CJFinal clonedPlayer = player.Clone() as CJFinal;
            clonedPlayer.Details.Charisma = 10;
            clonedPlayer.Details.Fitness = 10;

            // Lets see what happened to the original object
            Console.WriteLine("\nOriginal Object:");
            Console.WriteLine("Charisma: {0}, Fitness: {1}",
                player.Details.Charisma.ToString(),
                player.Details.Fitness.ToString());
            Console.WriteLine("\nICloneable Deep Cloned Object:");
            Console.WriteLine("Charisma: {0}, Fitness: {1}",
                clonedPlayer.Details.Charisma.ToString(),
                clonedPlayer.Details.Fitness.ToString());

            #endregion

        }
    }
}
