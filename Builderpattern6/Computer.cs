﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builderpattern6
{
    public enum ComputerType
    {
        Laptop,
        Desktop,
        Apple
    }

    public class Computer
    {
        private ComputerType Type;
        public string MotherBoard { get; set; }
        public string Processor { get; set; }
        public string Harddisk { get; set; }
        public string Screen { get; set; }

        public Computer(ComputerType Type_)
        {
            this.Type = Type_;
        }

        public void DisplayConfiguration()
        {
            string message;

            message = string.Format("Motherboard: {0}", MotherBoard);
            Console.WriteLine(message);

            message = string.Format("Processor: {0}", Processor);
            Console.WriteLine(message);

            message = string.Format("Harddisk: {0}", Harddisk);
            Console.WriteLine(message);

            message = string.Format("Screen: {0}", Screen);
            Console.WriteLine(message);

            Console.WriteLine();
        }
    }
}
