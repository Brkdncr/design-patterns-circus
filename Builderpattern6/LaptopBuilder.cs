﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builderpattern6
{
    public class LaptopBuilder : ComputerBuilder
    {

        public LaptopBuilder()
        {
            Computer = new Computer(ComputerType.Laptop);
        }

        public override void BuildMotherboard()
        {
            Computer.MotherBoard = "Dell";
        }

        public override void BuildProcessor()
        {
            Computer.Processor = "AMD Phenom";
        }

        public override void BuildHardDisk()
        {
            Computer.Harddisk = "SSD Disk";
        }

        public override void BuildScreen()
        {
            Computer.Screen = "15 Inch";
        }
    }
}
