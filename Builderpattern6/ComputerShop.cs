﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builderpattern6
{
    public class ComputerShop
    {
        public void ConstructComputer(ComputerBuilder Builder_)
        {
            Builder_.BuildMotherboard();
            Builder_.BuildProcessor();
            Builder_.BuildHardDisk();
            Builder_.BuildScreen();
        }
    }
}
