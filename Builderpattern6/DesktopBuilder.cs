﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builderpattern6
{
    public class DesktopBuilder : ComputerBuilder
    {
        public DesktopBuilder()
        {
            Computer = new Computer(ComputerType.Desktop);
        }

        public override void BuildMotherboard()
        {
            Computer.MotherBoard = "Acer";
        }

        public override void BuildProcessor()
        {
            Computer.Processor = "AMD";
        }

        public override void BuildHardDisk()
        {
            Computer.Harddisk = "120";
        }

        public override void BuildScreen()
        {
            Computer.Screen = "17 Inch";
        }
    }
}
