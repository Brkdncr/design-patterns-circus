﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builderpattern6
{
    class Program
    {
        static void Main(string[] args)
        {
            ComputerShop shop_ = new ComputerShop();
            AppleBuilder applePC_ = new AppleBuilder();
            shop_.ConstructComputer(applePC_);
            applePC_.Computer.DisplayConfiguration();

            Console.ReadLine();
        }
    }
}
