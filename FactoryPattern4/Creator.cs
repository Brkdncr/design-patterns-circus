﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern4
{
    public class Creator
    {
        public IProduct FactoryMethod(int Month_)
        {
            switch (Month_)
            {
                case 1:
                    return new ProductA();
                case 2:
                    return new ProductB();
                default:
                    return new DefaultProduct();
            }

        }
    }
}
