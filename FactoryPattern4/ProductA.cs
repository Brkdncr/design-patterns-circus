﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern4
{
   public class ProductA : IProduct
    {
        public string ShipFrom()
        {
            return "Product from Australia";
        }
    }
}
