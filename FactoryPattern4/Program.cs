﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern4
{
    class Program
    {
        static void Main(string[] args)
        {
            Creator creator_ = new Creator();
            IProduct prodA_ = creator_.FactoryMethod(3);
            prodA_.ShipFrom();
        }
    }
}
