﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern3
{
    class Program
    {
        static void Main(string[] args)
        {
            BuilderDirector director;

            director = new BuilderDirector((new HighSpeedNotebook()));
            NotebookProduct highSpeedNotebook = director.BuildNotebook();

            director = new BuilderDirector((new StandartNotebook()));
            NotebookProduct standartSpeedNotebook = director.BuildNotebook();

            Console.ReadLine();
        }
    }
}
