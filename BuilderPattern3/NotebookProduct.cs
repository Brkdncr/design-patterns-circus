﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern3
{
    public class NotebookProduct
    {
        public DisplayEnvironment ProductDisplayEnvironment { get; set; }
        public OEMEnvironment ProductOEMEnvironment { get; set; }
    }
}
