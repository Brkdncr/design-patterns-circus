﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern3
{
    class StandartNotebook : INotebookBuilder
    {
        public DisplayEnvironment BuildEnvironment()
        {
            return new DisplayEnvironment() { GraphicCard = "Standart Graphic Card", ScreenResolution = "648 x 760", ScreenWide = "12''" };
        }

        public OEMEnvironment BuildOEM()
        {
            return new OEMEnvironment() { Processor = "Standart Processor" };
        }
    }
}
