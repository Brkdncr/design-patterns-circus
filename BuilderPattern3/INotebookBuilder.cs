﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern3
{
    public interface INotebookBuilder
    {
        DisplayEnvironment BuildEnvironment();
        OEMEnvironment BuildOEM();
    }
}
