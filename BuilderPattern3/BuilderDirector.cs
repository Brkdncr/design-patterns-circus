﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern3
{
    public class BuilderDirector
    {
        private INotebookBuilder Builder;
        public BuilderDirector(INotebookBuilder builder)
        {
            Builder = builder;
        }

        public NotebookProduct BuildNotebook()
        {
            NotebookProduct product = new NotebookProduct();
            product.ProductDisplayEnvironment = Builder.BuildEnvironment();
            product.ProductOEMEnvironment = Builder.BuildOEM();
            return product;
        }

        public NotebookProduct BuildNotebookDisplayEnvironmentOnly()
        {
            NotebookProduct product = new NotebookProduct();
            product.ProductDisplayEnvironment = Builder.BuildEnvironment();
            return product;
        }

        public NotebookProduct BuildNotebookOEMEnvironmentOnly()
        {
            NotebookProduct product = new NotebookProduct();
            product.ProductOEMEnvironment = Builder.BuildOEM();
            return product;
        }
    }
}
