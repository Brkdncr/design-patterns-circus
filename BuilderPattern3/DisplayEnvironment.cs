﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern3
{
    public class DisplayEnvironment
    {
        public string GraphicCard { get; set; }
        public string ScreenWide { get; set; }
        public string ScreenResolution { get; set; }
    }
}
