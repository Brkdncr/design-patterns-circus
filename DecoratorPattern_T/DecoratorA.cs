﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern_T
{
    public class DecoratorA : IComponent
    {
        IComponent _component;

        public DecoratorA(IComponent c_)
        {
            this._component = c_;
        }
        public string Operation()
        {
            string s_ = this._component.Operation();
            s_ += " to school";
            return s_;
        }

        public string AddBehavior()
        {
            return "and i bought a coffea";
        }
    }
}
