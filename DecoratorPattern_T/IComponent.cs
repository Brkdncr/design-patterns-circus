﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern_T
{
    public interface IComponent
    {
        public string Operation();
    }
}
