﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern_T
{
    public class DecoratorB : IComponent
    {
        IComponent _component;

        public DecoratorB(IComponent c_)
        {
            this._component = c_;
        }
        public string Operation()
        {
            string s_ = _component.Operation();
            s_ += " to school";
            return s_;
        }

        public string AddedBehavior()
        {
            return " and i bought couppichino";
        }
    }
}
