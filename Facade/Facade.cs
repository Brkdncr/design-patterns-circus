﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    public class Facade
    {
        SubSystemA _SystemA;
        SubSystemB _SystemB;
        SubSystemC _SystemC;

        public Facade()
        {
            _SystemA = new SubSystemA();
            _SystemB = new SubSystemB();    
            _SystemC = new SubSystemC();
        }

        public void MethodOne()
        {
            Console.WriteLine("Method I");
            _SystemA.MethodA();
            _SystemB.MethodB();
            _SystemC.MethodC();
        }


    }
}
