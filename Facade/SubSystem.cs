﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class SubSystemA
    {
        public void MethodA()
        {
            Console.WriteLine("SubSystemA.MethodA");
        }
    }

    class SubSystemB
    {
        public void MethodB()
        {
            Console.WriteLine("SubSystemB.MethodB");
        }
    }

    class SubSystemC
    {
        public void MethodC()
        {
            Console.WriteLine("SubSystemC.MethodC");
        }
    }
}
