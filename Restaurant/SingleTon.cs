﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant
{
    public class SingleTon
    {
        public static SingleTon _Instance;
        private static object _Lock = new object();

        private SingleTon()
        {

        }

        public static SingleTon GetInstance()
        {
            if (_Instance == null)
            {
                lock (_Lock)
                {
                    if (_Instance == null)
                    {
                        _Instance = new SingleTon();
                    }
                }
            }
            return _Instance;
        }

    }
}
