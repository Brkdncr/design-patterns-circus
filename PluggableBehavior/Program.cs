﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluggableBehavior
{
    class Program
    {
        public static int TotalSelectedValues(int[] Array_, Func<int, bool> Selector_)
        {
            int total_ = 0;

            foreach (int item in Array_)
            {
                if (Selector_(item))
                {
                    total_ += item;
                }
            }
            return total_;
        }
        static void Main(string[] args)
        {
            int ret_ = 0;
            int[] myArray_ = new int[] { 1, 3, 5, 7, 9 };

            ret_ = TotalSelectedValues(myArray_, (value) => true);
            ret_ = TotalSelectedValues(myArray_, (value) => value > 5);
            ret_ = TotalSelectedValues(myArray_, (value) => value < 4);
        }
    }

}
