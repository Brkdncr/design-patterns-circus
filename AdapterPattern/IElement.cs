﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterPattern
{
    interface IElement
    {
        string Name { get; set; }
        int ElementID { get; set; }

        void Update();
    }


}
