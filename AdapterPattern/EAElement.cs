﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterPattern
{
    class EAElement
    {
        public string EAName { get; set; }
        public int EAElementID { get; set; }

        public void EAUpdate()
        {
            Console.WriteLine("EAUpdate {0} ID'li element update edildi.", EAElementID);
        }
    }
}
