﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            EAElement eaElement = new EAElement() { EAElementID = 123, EAName = "Test Element" };
            IElement element = new ElementAdapter(eaElement); // wtf is advantage ?
        }
    }
}
