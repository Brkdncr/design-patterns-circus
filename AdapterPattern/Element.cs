﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterPattern
{
    public class Element : IElement
    {
        private string _Name;
        private int _ElementID;

        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }

        public int ElementID
        {
            get
            {
                return _ElementID;
            }
            set
            {
                _ElementID = value;
            }
        }

        public void Update()
        {
            Console.WriteLine("Update {0} ID'li element update edildi.", ElementID);
        }
    }
}
