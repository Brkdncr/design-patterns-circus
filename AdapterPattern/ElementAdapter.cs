﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterPattern
{
    class ElementAdapter : IElement
    {
        private EAElement _EaElement;

        public ElementAdapter(EAElement EAElement_)
        {
            _EaElement = EAElement_;
        }

        public string Name
        {
            get
            {
                return _EaElement.EAName;
            }
            set
            {
                _EaElement.EAName = value;
            }
        }

        public int ElementID
        {
            get
            {
                return _EaElement.EAElementID;
            }
            set
            {
                _EaElement.EAElementID = value;
            }
        }

        public void Update()
        {
            _EaElement.EAUpdate();
        }
    }
}
