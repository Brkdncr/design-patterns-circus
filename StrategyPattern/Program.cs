﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculateClient minusClient = new CalculateClient(new Minus());
            Console.Write("<br />Minus: " + minusClient.Calculate(7, 1).ToString());
            CalculateClient plusClient = new CalculateClient(new Plussus());
            Console.Write("<br />Plussus: " + plusClient.Calculate(7, 1).ToString());
        }
    }
}
