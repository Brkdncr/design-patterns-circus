﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility3
{
    public abstract class PlayerHandler
    {
        protected PlayerHandler _NextHandler;
        public PlayerHandler NextHandler
        {
            set
            {
                _NextHandler = value;
            }
        }

        public abstract void Play(string FilePath_);
    }
}
