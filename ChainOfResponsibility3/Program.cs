﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility3
{
    class Program
    {
        static void Main(string[] args)
        {
            PlayerHandler mp4Player_ = new ConcreteHandlerMp4();
            PlayerHandler mpgPlayer_ = new ConcreteHandlerMpg();
            PlayerHandler aviPlayer_ = new ConcreteHandlerAvi();

            mp4Player_.NextHandler = mpgPlayer_;
            mpgPlayer_.NextHandler = aviPlayer_;

            mp4Player_.Play("video.mpg");
            mp4Player_.Play("video.avi");
            mp4Player_.Play("video.mp4");
            mp4Player_.Play("video.svf");

            Console.ReadLine();
        }
    }
}
