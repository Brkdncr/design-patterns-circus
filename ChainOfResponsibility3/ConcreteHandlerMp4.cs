﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility3
{
    class ConcreteHandlerMp4 : PlayerHandler
    {
        public override void Play(string FilePath_)
        {
            if (FilePath_.EndsWith(".mp4"))
                Console.WriteLine("MP4 playing...");
            else
                if (_NextHandler != null)
                    _NextHandler.Play(FilePath_);
        }
    }
}
