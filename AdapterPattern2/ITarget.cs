﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterPattern2
{
    public interface ITarget
    {
        List<string> GetEmployeeList();
    }
}
