﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern5
{
    public abstract class MemberBase
    {
        public abstract Member GetMember();
    }
}
