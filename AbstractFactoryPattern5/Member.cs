﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern5
{
    public abstract class Member
    {
        public abstract string Name { get; set; }

        public abstract string SurName { get; set; }
    }
}
