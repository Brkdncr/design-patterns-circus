﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern5
{
    public class GoldMemberCreator : MemberBase
    {
        private string _name;
        private string _surname;
        public GoldMemberCreator(string Name_, string Surname_)
        {
            this._name = Name_;
            this._surname = Surname_;
        }
        public override Member GetMember()
        {
            return new GoldMember(this._name, this._surname);
        }
    }
}
