﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern5
{
    class Program
    {
        static void Main(string[] args)
        {
            // create 'membercreator' class send MemberType enum as paramater
            Member goldMember_ = new GoldMemberCreator("TestGold", "TestGold").GetMember();
            Member silverMember_ = new SilverMemberCreator("TestSilver", "TestSilver").GetMember();
        }
    }
}
