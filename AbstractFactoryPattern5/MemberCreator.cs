﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern5
{
    public enum MEMBER_TYPE
    {
        Gold,
        Silver
    }
    public class MemberCreator
    {
        public static Member CreateMember(MEMBER_TYPE MemberType_, Member Member_)
        {
            Member newMember_ = null;
            switch (MemberType_)
            {
                case MEMBER_TYPE.Gold:
                    newMember_ = new GoldMember(Member_.Name, Member_.SurName);
                    break;
                case MEMBER_TYPE.Silver:
                    newMember_ = new SilverMember(Member_.Name, Member_.SurName);
                    break;
                default:
                    break;
            }
            return newMember_;
        }
    }
}
