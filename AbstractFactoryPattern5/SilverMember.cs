﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern5
{
    public class SilverMember : Member
    {
        private string _name;
        private string _surname;

        public SilverMember(string Name_, string Surname_)
        {
            this.Name = Name_;
            this.SurName = Surname_;
        }
        public override string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public override string SurName
        {
            get
            {
                return _surname;
            }
            set
            {
                _surname = value;
            }
        }
    }
}
