﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern5
{
    public class SilverMemberCreator : MemberBase
    {
        private string _name;
        private string _surname;

        public SilverMemberCreator(string Name_, string Surname_)
        {
            this._name = Name_;
            this._surname = Surname_;
        }

        public override Member GetMember()
        {
            return new SilverMember(this._name, this._surname);
        }
    }
}
