﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern5
{
    public class GoldMember : Member
    {
        private string _name;
        private string _surname;

        public GoldMember(string Name_, string Surname_)
        {
            this._name = Name_;
            this._surname = Surname_;
        }

        public override string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public override string SurName
        {
            get
            {
                return _surname;
            }
            set
            {
                _surname = value;
            }
        }
    }
}
