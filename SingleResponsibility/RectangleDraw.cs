﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SingleResponsibility
{
    public class RectangleDraw
    {
        public void Draw(Form form, RectangleShape rectangleShape)
        {
            Brush myBrush = null;
            Graphics formGraphics = form.CreateGraphics();
            formGraphics.FillRectangle(myBrush,
            new Rectangle(0, 0, rectangleShape.Width, rectangleShape.Height));
        }
    }
}
