﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern
{
    class Program
    {
        static void Main(string[] args)
        {
           Server server = new Server();
            server.AddProgram(20);
            server.AddProgram(10);
            server.AddProgram(30);

            Console.ReadLine();  
        }
    }
}
