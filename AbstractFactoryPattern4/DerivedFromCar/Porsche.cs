﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern4
{
    public class Porsche : Car
    {
        private readonly string _company;
        private string _model;
        private int _speed;

        public Porsche(string Model_, int Speed_)
        {
            this._company = this.GetType().Name;
            this._model = Model_;
            this._speed = Speed_;
        }
        public override string Company
        {
            get { return _company; }
        }

        public override string Model
        {
            get
            {
                return _model;
            }
            set
            {
                _model = value;
            }
        }

        public override int Speed
        {
            get
            {
                return _speed;
            }
            set
            {
                _speed = value;
            }
        }
    }
}
