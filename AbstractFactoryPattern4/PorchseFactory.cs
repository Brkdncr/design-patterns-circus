﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern4
{
    public class PorchseFactory : CarFactory
    {
        private string _model;
        private int _speed;

        public PorchseFactory(string Model_, int Speed_)
        {
            this._model = Model_;
            this._speed = Speed_;
        }
        public override Car GetCar(string Model_, int Speed_)
        {
            return new Porsche(this._model, this._speed);
        }
    }
}
