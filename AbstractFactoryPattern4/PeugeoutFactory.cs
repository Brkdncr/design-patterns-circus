﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern4
{
    public class PeugeoutFactory : CarFactory
    {
        private string _model;
        private int _speed;
        public PeugeoutFactory(string Model_,int Speed_)
        {
            this._model = Model_;
            this._speed = Speed_;
        }

        public override Car GetCar()
        {
            return new Peugeout(_model, _speed);
        }
    }
}
