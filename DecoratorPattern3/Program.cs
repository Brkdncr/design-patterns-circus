﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern3
{
    class Program
    {
        static void Main(string[] args)
        {
            CakeBase cBase = new CakeBase("Kek",300);
            //PrintProductDetails(cBase);
            // Let now add a Cherry on it
            CherryDecorator cherryCake = new CherryDecorator(cBase);
            // Lets now add Scent to it
            ArtificialScentDecorator scentedCake = new ArtificialScentDecorator(cherryCake);
        
        }
    }
}
