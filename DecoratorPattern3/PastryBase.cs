﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern3
{
    class PastryBase : BakeryComponent
    {
        private string _name;
        private double _price;

        public PastryBase(string Name_, double Price_)
        {
            _name = Name_;
            _price = Price_;
        }
        public override string GetName()
        {
            return _name;
        }

        public override double GetPrice()
        {
            return _price;
        }
    }
}
