﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern3
{
    class ArtificialScentDecorator : Decorator
    {
        public ArtificialScentDecorator(BakeryComponent Comp_)
            : base(Comp_)
        {
            this.m_Name = "Artifical Scent";
            this.m_Price = 3.0;
        }
    }
}
