﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern3
{
    public abstract class BakeryComponent
    {
        public abstract string GetName();
        public abstract double GetPrice();
    }
}
