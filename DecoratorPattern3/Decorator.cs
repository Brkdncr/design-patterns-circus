﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern3
{
    public class Decorator : BakeryComponent
    {
        private BakeryComponent _Component = null;
        protected string m_Name = "Undefined Decorator";
        protected double m_Price = 0.0;

        public Decorator(BakeryComponent Comp_)
        {
            this._Component = Comp_;
        }
        public override string GetName()
        {
            return string.Format("{0}, {1}", _Component.GetName(), m_Name);
        }

        public override double GetPrice()
        {
            return m_Price + _Component.GetPrice();
        }
    }
}
