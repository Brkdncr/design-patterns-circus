﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Creator
    {
        public enum CalcType
        {
            Toplama,
            Cikarma
        }

        public Calculate CalcFactory(CalcType Type_)
        {
            Calculate calculate_ = null;
            switch (Type_)
            {
                case CalcType.Toplama:
                    calculate_ = new Toplama();
                    break;
                case CalcType.Cikarma:
                    calculate_ = new Cikarma();
                    break;
                default:
                    break;
            }
            return calculate_;
        }
    }
}
