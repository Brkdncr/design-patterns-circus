﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnTopla_Click(object sender, EventArgs e)
        {
            Creator creator_ = new Creator();
            Calculate Calculator_ = creator_.CalcFactory(Creator.CalcType.Toplama);
            Calculator_.CalculateReturn(3, 5);
        }

        private void btnCıkar_Click(object sender, EventArgs e)
        {
            Creator creator_ = new Creator();
            Calculate Calculator_ = creator_.CalcFactory(Creator.CalcType.Cikarma);
            Calculator_.CalculateReturn(3, 5);
        }
    }
}
