﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class Singleton
    {
        private Singleton()
        {

        }
        private static object _Lock;
        private static Singleton _SingleTon;

        public static Singleton GetInstance()
        {
            if (_SingleTon == null)
            {
                lock (_Lock)
                {
                    if (_SingleTon == null)
                        _SingleTon = new Singleton();
                }
            }
            return _SingleTon;
        }
    }
}
