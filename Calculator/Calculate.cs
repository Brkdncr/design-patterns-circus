﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public abstract class Calculate
    {
        public abstract int CalculateReturn(int Input1_, int Input2_);
    }

    public class Toplama : Calculate
    {
        public override int CalculateReturn(int Input1_, int Input2_)
        {
            return Input1_ + Input2_;
        }
    }

    public class Cikarma : Calculate
    {
        public override int CalculateReturn(int Input1_, int Input2_)
        {
            return Input1_ - Input2_;
        }
    }

}
