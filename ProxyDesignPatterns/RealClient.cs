﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyDesignPatterns
{
    class RealClient : IClient
    {
        string Data;

        public RealClient()
        {
            Console.WriteLine("Real Client initialized.");
            Data = "DotNet Tricks";
        }

        public string GetData()
        {
            return Data;
        }
    }
}
