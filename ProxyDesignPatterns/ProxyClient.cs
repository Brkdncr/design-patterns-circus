﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyDesignPatterns
{
    class ProxyClient : IClient
    {
        RealClient realClient_ = new RealClient();

        public ProxyClient()
        {
            Console.WriteLine("Proxy Client :: initialized.");
        }

        public string GetData()
        {
           return realClient_.GetData();
        }
    }
}
