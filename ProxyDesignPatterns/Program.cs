﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyDesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            ProxyClient proxy_ = new ProxyClient();
            Console.WriteLine("Data from proxy client : " + proxy_.GetData());
            Console.ReadKey();
        }
    }
}
