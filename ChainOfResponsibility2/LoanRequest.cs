﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility2
{
    class LoanRequest
    {
        public string Customer { get; set; }
        public decimal Amount { get; set; }
    }
}
