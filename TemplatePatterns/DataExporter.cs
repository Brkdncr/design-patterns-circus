﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplatePatterns
{
    public abstract class DataExporter
    {
        public void ReadData()
        {
            Console.WriteLine("Reading the data from SqlServer");
        }

        public void FormatData()
        {
            Console.WriteLine("Formating the data as per requriements.");
        }

        public abstract void ExportData();

        public void ExportFormatedData()
        {
            this.ReadData();
            this.FormatData();
            this.ExportData();
        }

    }
}
