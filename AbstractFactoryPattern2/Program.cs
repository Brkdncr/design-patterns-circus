﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern2
{
    class Program
    {
        static void Main(string[] args)
        {
            Factory factoryPC1_ = new Factory(new Concrete1Factory());
            factoryPC1_.MergeAll();

            Factory factoryPC2_ = new Factory(new Concrete1Factory());
            factoryPC2_.MergeAll();

            /*
            PcFactory pcConcreate1_ = new Concrete1Factory();
            pcConcreate1_.CreateHarddisk();
            pcConcreate1_.CreateRam();

            PcFactory pcConcreate2_ = new Concrete2Factory();
            pcConcreate2_.CreateHarddisk();
            pcConcreate2_.CreateRam();
             * */
        }
    }
}
