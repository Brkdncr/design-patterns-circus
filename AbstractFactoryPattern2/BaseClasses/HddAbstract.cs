﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern2
{
    public abstract class HddAbstract
    {
        public abstract void HddIslem();
        public abstract string HddTur { get; }

    }
}
