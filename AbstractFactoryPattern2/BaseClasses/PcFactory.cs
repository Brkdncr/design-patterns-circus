﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern2
{
    public abstract class PcFactory
    {
        public abstract HddAbstract CreateHarddisk();
        public abstract RamAbstract CreateRam();
    }
}
