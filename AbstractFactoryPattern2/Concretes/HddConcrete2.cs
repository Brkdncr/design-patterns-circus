﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern2
{
    public class HddConcrete2 : HddAbstract
    {
        public override void HddIslem()
        {
            Console.WriteLine("HddConcrete2 merged");
        }

        public override string HddTur
        {
            get { return "Hdd type: HddConcrete2"; }
        }
    }
}
