﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern2
{
    public class HddConcrete1 : HddAbstract
    {
        public override void HddIslem()
        {
            Console.WriteLine("HddConcrete1 merged.");
        }

        public override string HddTur
        {
            get { return "this hdd type : HddConcrete1"; }
        }
    }
}
