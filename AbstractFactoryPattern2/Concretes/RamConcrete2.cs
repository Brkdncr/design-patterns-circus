﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern2
{
    public class RamConcrete2 : RamAbstract
    {
        public override void RamIslem()
        {
            Console.WriteLine("RamConcrete2 merged");
        }

        public override string RamTur
        {
            get { return "Ram type: RamConcrete2"; }
        }
    }
}
