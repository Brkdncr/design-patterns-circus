﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern2
{
    //Casper PC
    class Concrete1Factory : PcFactory
    {
        public override HddAbstract CreateHarddisk()
        {
            return new HddConcrete1();
        }

        public override RamAbstract CreateRam()
        {
            return new RamConcrete1();
        }
    }
}
