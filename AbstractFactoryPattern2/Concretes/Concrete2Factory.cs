﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern2
{
    //Dell Pc
    public class Concrete2Factory : PcFactory
    {
        public override HddAbstract CreateHarddisk()
        {
            return new HddConcrete2();
        }

        public override RamAbstract CreateRam()
        {
            return new RamConcrete2();
        }
    }
}
