﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern2
{
    class RamConcrete1 : RamAbstract
    {
        public override void RamIslem()
        {
            Console.WriteLine("RamConcrete1 merged");
        }

        public override string RamTur
        {
            get { return "Ram type: RamConcrete1"; }
        }
    }
}
