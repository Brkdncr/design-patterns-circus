﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern2
{
    public class Factory
    {
        private PcFactory _PcFactory;
        private HddAbstract _HddAbstract;
        private RamAbstract _RamAbstract;

        public Factory(PcFactory PcFactory_)
        {
            _PcFactory = PcFactory_;
            _HddAbstract = _PcFactory.CreateHarddisk();
            _RamAbstract = _PcFactory.CreateRam();
        }
        public void MergeAll()
        {
            Console.WriteLine(_HddAbstract.HddTur);
            _HddAbstract.HddIslem();
            Console.WriteLine(_RamAbstract.RamTur);
            _RamAbstract.RamIslem();
        }

    }
}
