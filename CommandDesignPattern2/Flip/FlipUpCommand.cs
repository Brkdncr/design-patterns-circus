﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandDesignPattern2
{
    public class FlipUpCommand : ICommand
    {
        private Light _Light;

        public FlipUpCommand(Light Light_)
        {
            _Light = Light_;
        }

        public void Execute()
        {
            _Light.TurnOn();
        }
    }
}
