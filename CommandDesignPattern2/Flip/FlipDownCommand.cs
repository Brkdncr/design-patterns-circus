﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandDesignPattern2
{
    public class FlipDownCommand : ICommand
    {
        private Light _Light;

        public FlipDownCommand(Light Light_)
        {
            _Light = Light_;
        }

        public void Execute()
        {
            _Light.TurnOff();
        }
    }
}
