﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandDesignPattern2
{
    class Program
    {
        static void Main(string[] args)
        {
            Light lamp_ = new Light();
            ICommand switchUp = new FlipUpCommand(lamp_);
            ICommand switchDown = new FlipDownCommand(lamp_);

            Switch s_ = new Switch();

            // ON Lamp
            s_.StoreAndExecute(switchUp);
            // OFF Lamp
            s_.StoreAndExecute(switchDown);

            Console.ReadKey();
        }
    }
}
