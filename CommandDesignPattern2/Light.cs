﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandDesignPattern2
{
    public class Light
    {
        public void TurnOn()
        {
            Console.WriteLine("Lights ON");
        }

        public void TurnOff()
        {
            Console.WriteLine("Lights OFF");
        }
    }
}
