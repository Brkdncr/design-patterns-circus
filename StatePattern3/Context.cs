﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern3
{
    public class Context
    {
        public StateBase State { get; set; }

        public Context(StateBase State_)
        {
            State = State_;
        }

        public void Request()
        {
            State.Change(this);
        }
    }
}
