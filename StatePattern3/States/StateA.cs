﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern3
{
    public class StateA : StateBase
    {
        public void Change(Context Context)
        {
            Context.State = new StateB();
        }
    }
}
