﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern3
{
    public class StateC : StateBase
    {
        public void Change(Context Context)
        {
            Context.State = new StateA();
        }
    }
}
