﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern3
{
    public interface StateBase
    {
        void Change(Context Context);
    }
}
