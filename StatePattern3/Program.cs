﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern3
{
    class Program
    {
        static void Main(string[] args)
        {
            Context Context_ = new Context(new StateA());

            Context_.Request();
            Context_.Request();
            Context_.Request();
        }
    }
}
