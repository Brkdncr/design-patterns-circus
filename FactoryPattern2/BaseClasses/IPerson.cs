﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern2
{
    public interface IPerson
    {
        int Add(Person P_);
        List<Person> Find(string Name_, string Surname_);
    }
}
