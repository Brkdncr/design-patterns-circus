﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern2
{
    class Program
    {
        static void Main(string[] args)
        {
            IPerson Engineer_ = ResourceFactory.CreateResource(ResourceType.EngineerDepartment);
            IPerson Sales_ = ResourceFactory.CreateResource(ResourceType.SalesDepartment);
        }
    }
}
