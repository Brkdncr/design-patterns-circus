﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern2
{
    public enum ResourceType
    {
        EngineerDepartment,
        SalesDepartment
    }
    public static class ResourceFactory
    {
        public static IPerson CreateResource(ResourceType Type_)
        {
            IPerson person_ = null;
            switch (Type_)
            {
                case ResourceType.EngineerDepartment:
                    person_ = new EngineerDepartment();
                    break;
                case ResourceType.SalesDepartment:
                    person_ = new SalesDepartment();
                    break;
                default:
                    break;
            }
            return person_;
        }

    }
}
