﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CascadePattern
{
    public class Mail
    {
        public Mail To(string Address_) { return this; }
        public Mail From(string From_) { return this; }
        public Mail Subject(string Subject_) { return this; }
        public Mail Message(string Message_) { return this; }
        public void Send() {}
    }
}
