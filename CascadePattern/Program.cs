﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CascadePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            new Mail()
                .To("burakkdincer@gmail.com")
                .From("Burakd@hotmail.com")
                .Subject("Test mail")
                .Message("Message")
                .Send();
        }
    }
}
