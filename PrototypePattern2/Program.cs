﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create two instances and clone each 
            ConcreatePrototype p1 = new ConcreatePrototype(1);
            ConcreatePrototype c1 = (ConcreatePrototype)p1.Clone();
            Console.WriteLine("Cloned: {0}", c1.ID);

            ConcretePrototype2 p2 = new ConcretePrototype2(2);
            ConcretePrototype2 c2 = (ConcretePrototype2)p2.Clone();
            Console.WriteLine("Cloned: {0}", c2.ID);

            // Wait for user 
            Console.Read();
        }
    }
}
