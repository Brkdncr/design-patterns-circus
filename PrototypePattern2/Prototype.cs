﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern2
{
    public abstract class Prototype
    {
        private int _ID;

        public int ID
        {
            get { return _ID; }
        }

        public Prototype(int ID_)
        {
            this._ID = ID_;
        }

        public abstract Prototype Clone();
    }
}
