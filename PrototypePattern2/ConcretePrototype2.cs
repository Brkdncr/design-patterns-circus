﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern2
{
    public class ConcretePrototype2 : Prototype
    {
        public ConcretePrototype2(int ID_)
            : base(ID_)
        {

        }
        public override Prototype Clone()
        {
            return (Prototype)this.MemberwiseClone();
        }
    }
}
