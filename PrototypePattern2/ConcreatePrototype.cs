﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern2
{
    public class ConcreatePrototype : Prototype
    {
        public ConcreatePrototype(int ID_)
            : base(ID_)
        {

        }
        public override Prototype Clone()
        {
            // Shallow copy 
            return (Prototype)this.MemberwiseClone();
        }
    }
}
