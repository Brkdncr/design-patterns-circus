﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern5
{
    public abstract class Tablet
    {
        private string _Model;
        private string _Brand;

        public Tablet(string Model_, string Brand_)
        {
            this._Model = Model_;
            this._Brand = Brand_;
        }

        public abstract void Accept(IVisitor Visit_);
    }
}
