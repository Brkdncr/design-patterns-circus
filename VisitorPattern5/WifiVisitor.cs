﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern5
{
    public class WifiVisitor : IVisitor
    {
        public void Visit(Tablet Tablet_)
        {
            if (Tablet_ is IPad)
                Console.WriteLine("Ipad wifi has open.");
            else if (Tablet_ is GalaxyTab)
                Console.WriteLine("GalaxyTab does not have wifi option.");
            else
                Console.WriteLine("This object is not a tablet.");  
        }
    }
}
