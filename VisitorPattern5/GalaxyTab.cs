﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern5
{
    public class GalaxyTab : Tablet
    {
        public GalaxyTab(string Model_, string Brand_)
            : base(Model_, Brand_)
        {

        }
        public override void Accept(IVisitor Visit_)
        {
            Visit_.Visit(this);
        }
    }
}
