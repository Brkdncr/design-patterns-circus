﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern5
{
    public class IPad : Tablet
    {
        public IPad(string Brand_, string Model_)
            : base(Model_, Brand_)
        {

        }
        public override void Accept(IVisitor Visit_)
        {
            Visit_.Visit(this);
        }
    }
}
