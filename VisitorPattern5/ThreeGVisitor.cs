﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern5
{
    public class ThreeGVisitor : IVisitor
    {
        public void Visit(Tablet Tablet_)
        {
            if (Tablet_ is IPad)
                Console.WriteLine("Ipad wifi does not have 3G option.");
            else if (Tablet_ is GalaxyTab)
                Console.WriteLine("GalaxyTab 3G has open.");
            else
                Console.WriteLine("This object is not a tablet.");
        }
    }  
}
