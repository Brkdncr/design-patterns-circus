﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern5
{
    public interface IVisitor
    {
        void Visit(Tablet Tablet_);
    }
}
