﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern
{
    enum ScreenTypes
    {
        WebScreen,
        MobileScreen,
        WindowsScreen
    }
    public class Creator
    {
        public Screen GetScreen(ScreenTypes Type_)
        {
            Screen Screen_ = null;

            switch (Type_)
            {
                case ScreenTypes.WebScreen:
                    Screen_ = new WebScreen();
                    break;
                case ScreenTypes.MobileScreen:
                    Screen_ = new MobileScreen();
                    break;
                case ScreenTypes.WindowsScreen:
                    Screen_ = new WindowsScreen();
                    break;
                default:
                    break;
            }
            return Screen_;
        }
    }

    public abstract class ScreenCreator
    {
        public abstract Screen GetScreen();
    }

    public class WebCreator : ScreenCreator
    {
        public override Screen GetScreen()
        {
            return new WebScreen();
        }
    }

    public class WindowsCreator : ScreenCreator
    {
        public override Screen GetScreen()
        {
            return new WindowsScreen();
        }
    }


}
