﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern
{
    public abstract class Screen
    {
        public abstract void Draw();
    }

    public class WebScreen : Screen
    {
        public override void Draw()
        {
            Console.WriteLine("WebScreen");
        }
    }

    public class WindowsScreen : Screen
    {
        public override void Draw()
        {
            Console.WriteLine("WindowsScreen");
        }
    }

    public class MobileScreen : Screen
    {
        public override void Draw()
        {
            Console.WriteLine("MobileScreen");
        }
    }



}
