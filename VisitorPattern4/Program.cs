﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern4
{
    class Program
    {
        static void Main(string[] args)
        {
            CBulldog myDog_ = new CBulldog();
            myDog_.Bark();
            Console.WriteLine(myDog_.MyAge.ToString());

            AgeVisitor av_ = new AgeVisitor();
            myDog_.TakeVisitor(av_);

            BarkVisitor bv = new BarkVisitor();
            myDog_.TakeVisitor(bv);

            myDog_.Bark();

        }
    }
}
