﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern4
{
    public class AgeVisitor : BaseVisitor
    {
        public AgeVisitor()
            : base()
        {
        }
        public override void VisitBullDog(CBulldog BullDog_)
        {
            BullDog_.MyAge = 5;
        }

    }
}
