﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern4
{
    public class CBulldog
    {
        private int _Age;
        private string _BarkString;
        public int MyAge
        {
            get
            {
                return _Age;
            }
            set
            {
                _Age = value;
            }
        }
        public string MyBark
        {
            get
            {
                return _BarkString;
            }
            set
            {
                _BarkString = value;
            }
        }

        public void TakeVisitor(BaseVisitor Visitor_)
        {
            Visitor_.VisitBullDog(this);
        }

        public CBulldog()
        {
            _Age = 15;
            _BarkString = "Woof";
        }
        public void Bark()
        {
            Console.WriteLine(_BarkString);
        }
    }
}
