﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern4
{
    public class BaseVisitor
    {
        public BaseVisitor()
        {

        }

        public virtual void VisitBullDog(CBulldog BullDog_)
        {

        }
    }
}
