﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern3
{
    class Program
    {
        static void Main(string[] args)
        {
          Position posManager_ =  Factory.GetPosition(PositionType.Manager);
          Console.WriteLine(posManager_.Title); 
        }
    }
}
