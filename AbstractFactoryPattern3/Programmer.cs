﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern3
{
    class Programmer : Position
    {
        public override string Title
        {
            get { return "Programmer"; }
        }
    }
}
