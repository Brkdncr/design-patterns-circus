﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern3
{
    public enum PositionType
    {
        Clerk,
        Manager,
        Programmer
    }
    public class Factory
    {
        public static Position GetPosition(PositionType PositionType_)
        {
            Position position_ = null;
            switch (PositionType_)
            {
                case PositionType.Clerk:
                    position_ = new Clerk();
                    break;
                case PositionType.Manager:
                    position_ = new Manager();
                    break;
                case PositionType.Programmer:
                    position_ = new Programmer();
                    break;
                default:
                    break;
            }
            return position_;
        }
    }
}
