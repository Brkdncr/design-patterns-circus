﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandDesignPattern
{
    class Client
    {
        public void RunCommand()
        {
            Invoker invoker_ = new Invoker();
            Receiver receiver_ = new Receiver();
            ConcreteCommand command = new ConcreteCommand(receiver_);
            command.Parameter = "Dot Net Tricks !!";
            invoker_.Command = command;
            invoker_.ExecuteCommand();
        }
    }
}
