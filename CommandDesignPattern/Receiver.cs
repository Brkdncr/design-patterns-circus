﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandDesignPattern
{
    class Receiver
    {
        public void Action(string Message_)
        {
            Console.WriteLine("Action called with message {0}", Message_);
        }
    }
}
