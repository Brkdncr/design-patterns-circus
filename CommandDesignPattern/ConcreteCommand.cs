﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandDesignPattern
{
    class ConcreteCommand : ICommand
    {
        protected Receiver _Receiver;
        public string Parameter { get; set; }

        public ConcreteCommand(Receiver Receiver_)
        {
            _Receiver = Receiver_;
        }

        public void Execute()
        {
            _Receiver.Action(Parameter);
        }
    }
}
