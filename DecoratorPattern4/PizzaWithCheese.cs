﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern4
{
    public class PizzaWithCheese : IPizza
    {
        private IPizza _pizza;
        private int _priceofCheese;

        public PizzaWithCheese(IPizza Pizza_, int PriceOfCheese_)
        {
            _pizza = Pizza_;
            _priceofCheese = PriceOfCheese_;
        }
        public int GetPrice()
        {
            return _pizza.GetPrice() + _priceofCheese;
        }
    }
}
