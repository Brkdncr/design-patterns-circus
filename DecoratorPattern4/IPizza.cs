﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern4
{
    public interface IPizza
    {
        int GetPrice();
    }
}
