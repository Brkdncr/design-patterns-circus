﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern4
{
    public class PizzaWithChicken : IPizza
    {
        private IPizza _pizza;
        private int _priceOfChicken;

        public PizzaWithChicken(IPizza Pizza_, int Price_)
        {
            this._pizza = Pizza_;
            this._priceOfChicken = Price_;
        }

        public int GetPrice()
        {
            return _pizza.GetPrice() + _priceOfChicken;
        }
    }
}
