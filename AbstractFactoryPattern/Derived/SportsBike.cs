﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern
{
    class SportsBike : Bike
    {
        public string Name()
        {
            return "Sports Bike- Name";
        }
    }
}
