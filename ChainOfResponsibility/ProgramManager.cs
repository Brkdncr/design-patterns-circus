﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    public class ProgramManager : Employee
    {
        void ProgramManager_onLeaveApplied(int RequestDays)
        {
            ApproveLeave(RequestDays);
            if (NextApprover != null)
                NextApprover.ApplyLeave(RequestDays);
        }

        // If we can process lets show the output  
        public override void ApproveLeave(int requestedDays)
        {
            Console.WriteLine(@"EmployeeId: {0} Requested Days: {1}   
             Approve: {2}",
                this.EmployeeId, requestedDays, "ProgramManager");
        }
    }
}
