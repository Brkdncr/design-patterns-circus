﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    public class Engineer : Employee
    {
        public Engineer()
        {
            this.onLeaveApplied += Engineer_onLeaveApplied;
        }

        void Engineer_onLeaveApplied(int RequestDays)
        {
            if (NextApprover != null)
            {
                NextApprover.ApplyLeave(RequestDays);
            }  
        }

        public override void ApproveLeave(int requestedDays)
        {
            Console.WriteLine(@"Engineer does not have  
                   approve permission.");
        }  

    }
}
