﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    public abstract class Employee
    {
        public Guid EmployeeId { get; set; }
        public int LeaveDays { get; set; }
        // approve edecek bir sonraki kisi  
        public Employee NextApprover { get; set; }

        public Employee()
        {
            EmployeeId = new Guid();
            LeaveDays = 15;
        }

        public delegate void OnLeaveApplied(int RequestDays);
        public event OnLeaveApplied onLeaveApplied = null;

        public abstract void ApproveLeave(int requestedDays);

        public void ApplyLeave(int requestedDays)
        {
            if (onLeaveApplied != null)
            {
                onLeaveApplied(requestedDays);
            }
        }

    }
}
