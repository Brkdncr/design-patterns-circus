﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    class Program
    {
        static void Main(string[] args)
        {
            Engineer engineer_ = new Engineer();
            ProgramManager pManager_ = new ProgramManager();
            HR hr_ = new HR();

            engineer_.NextApprover = pManager_;
            pManager_.NextApprover = hr_;
            engineer_.ApplyLeave(3);
            Console.ReadLine();
        }
    }
}
