﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern2
{
    class Program
    {
        static void Main(string[] args)
        {
            IElement houseHold_ = new Household("dedde");
            IVisitor visitor_ = new SantaClaus();
            visitor_.Visit(houseHold_);
        }
    }
}
