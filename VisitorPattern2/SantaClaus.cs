﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern2
{
    class SantaClaus : IVisitor
    {
        public void Visit(IElement element_)
        {
            Console.WriteLine("Santa Visited.. " + element_._Name);
        }
    }
}
