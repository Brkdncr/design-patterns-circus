﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern2
{
    class Household : IElement
    {
        private string Name;
        string IElement._Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        }

        public Household(string Name_)
        {
            Name = Name_;
        }

        public void Accept(IVisitor visitor_)
        {
            visitor_.Visit(this);
        }
    }
}
