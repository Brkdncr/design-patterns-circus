﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern2
{
    class BusinessEntity : IElement
    {
        private string Name;
        public string _Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        }

        public void Accept(IVisitor visitor_)
        {
            visitor_.Visit(this);
        }
    }
}
