﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern2
{
    public interface IElement
    {
        string _Name { get; set; }
        void Accept(IVisitor visitor_);
    }
}
