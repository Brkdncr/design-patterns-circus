﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPattern.FactoryPatternGE;
using FactoryPattern;

namespace DesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Creator creator_ = new Creator();
            Screen screenWindow_ = creator_.ScreenFactory(ScreenModel.Windows);
            Screen screenWeb_ = creator_.ScreenFactory(ScreenModel.Web);
            Screen screenMobile_ = creator_.ScreenFactory(ScreenModel.Mobile);

            screenWindow_.Draw();
            screenWeb_.Draw();
            screenMobile_.Draw();
        }
    }
}
