﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.FactoryPatternGE
{
    public abstract class Screen
    {
        public abstract void Draw();
    }

    public class WinScreen : Screen
    {
        public override void Draw()
        {
            Console.WriteLine("Windows screen");
        }
    }

    public class WebScreen : Screen
    {
        public override void Draw()
        {
            Console.WriteLine("Web screen");
        }
    }

    public class MobileScreen : Screen
    {
        public override void Draw()
        {
            Console.WriteLine("Mobile screen");
        }
    }



}
