﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.FactoryPatternGE
{
    enum ScreenModel
    {
        Windows,
        Web,
        Mobile
    }
    class Creator
    {
        public Screen ScreenFactory(ScreenModel ScreenModel_)
        {
            Screen screen_ = null;
            switch (ScreenModel_)
            {
                case ScreenModel.Windows:
                    screen_ = new WinScreen();
                    break;
                case ScreenModel.Web:
                    screen_ = new WebScreen();
                    break;
                case ScreenModel.Mobile:
                    screen_ = new MobileScreen();
                    break;
                default:
                    break;
            }
            return screen_;
        }
    }
}
