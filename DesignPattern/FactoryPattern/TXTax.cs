﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.FactoryPattern
{
    public class TXTax : ITax
    {
        public decimal CalculateTax(decimal total)
        {
            return total * .08m;
        }
    }
}
