﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPattern.FactoryPattern;

namespace FactoryPattern
{
    public class OrderItem
    {
        public int Quantity;
        public string Code;
        public decimal Cost;
    }

    public class Customer
    {
        public string StateCode;
        public string ZipCode;
        public string County;
    }

    public class Order
    {
        ITaxFactory _taxFactory;

        public Order(Customer c)
            : this(new DateBasedTaxFactory(c, new CustomerBasedTaxFactory(c)))
        {

        }

        public Order(ITaxFactory taxFactory)
        {
            _taxFactory = taxFactory;
        }
        List<OrderItem> _orderItems = new List<OrderItem>();
        public decimal CalculateTotal(Customer customer)
        {
            decimal total = _orderItems.Sum((item) =>
            {
                return item.Cost * item.Quantity;
            });

            total = total + _taxFactory.GetTaxObject().CalculateTax(customer, total);
            return total;
        }


    }
}
