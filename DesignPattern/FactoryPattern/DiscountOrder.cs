﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.FactoryPattern
{
    public class DiscountOrder : IOrder
    {
        IOrder _order;
        public DiscountOrder(IOrder order)
        {
            _order = order;
        }

        public decimal CalculateTotal()
        {
            return _order.CalculateTotal() - 4;
        }
        public int NumberOfItems()
        {
            return _order.NumberOfItems();
        }
    }
}
