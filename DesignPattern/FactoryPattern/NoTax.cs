﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryPattern;

namespace DesignPattern.FactoryPattern
{
    public class NoTax : ITax
    {
        public decimal CalculateTax(Customer customer, decimal total)
        {
            return 0.0m;
        }
    }
 
}
