﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryPattern;

namespace DesignPattern.FactoryPattern
{
    public interface ITax
    {
        decimal CalculateTax(Customer customer, decimal total);
    }
}
