﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryPattern;

namespace DesignPattern.FactoryPattern
{
    public class DateBasedTaxFactory : ITaxFactory
    {
        Customer _customer;
        ITaxFactory _taxFactory;

        public DateBasedTaxFactory(Customer c, ITaxFactory cb)
        {
            _customer = c;
            _taxFactory = cb;
        }
        public ITax GetTaxObject()
        {
            if (_customer.StateCode == "TX" && DateTime.Now.Month == 4 && DateTime.Now.Day == 4)
            {
                return new NoTax();
            }
            else
                return _taxFactory.GetTaxObject();
        }
    }
}
