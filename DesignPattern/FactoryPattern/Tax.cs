﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryPattern;

namespace DesignPattern.FactoryPattern
{
    public class Tax : ITax
    {
        public decimal CalculateTax(Customer customer, decimal total)
        {
            decimal tax;
            if (customer.StateCode == "TX")
                tax = total * .08m;
            else if (customer.StateCode == "FL")
                tax = total * .09m;
            else
                tax = .03m;
            return tax;
        }
    }
}
