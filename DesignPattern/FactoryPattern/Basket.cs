﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryPattern;

namespace DesignPattern.FactoryPattern
{
    public class Basket
    {
        Order _order;
        public Basket(int sessionId, IOrderFactory orderFactory)
        {
            int orderId = 3333;//Session[sessionId].OrderId;
            _order = orderFactory.GetOrder(orderId);
        }
        public Basket(int sessionId)
            : this(sessionId, new OrderFactory())
        {
        }
    }
}
