﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryPattern;

namespace DesignPattern.FactoryPattern
{
    public class OrderFactory : IOrderFactory
    {
        public IOrder GetOrder(int orderId)
        {
            IOrder order = null;//get from repository or wherever
            return new ShippingOrder(new DiscountOrder(order));
        }
    }
}
