﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.FactoryPattern
{
    public class ShippingOrder : IOrder
    {
        IOrder _order;
        public ShippingOrder(IOrder order)
        {
            _order = order;
        }

        public decimal CalculateTotal()
        {
            decimal shippingCost = 1.3m * _order.NumberOfItems();
            return shippingCost + _order.CalculateTotal();
        }
        public int NumberOfItems()
        {
            return _order.NumberOfItems();
        }
    }  
}
