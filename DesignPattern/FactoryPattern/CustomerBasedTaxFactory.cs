﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryPattern;

namespace DesignPattern.FactoryPattern
{
    public class CustomerBasedTaxFactory : ITaxFactory
    {
        Customer _customer;
        static Dictionary<string, ITax> stateTaxObjects = new Dictionary<string, ITax>();

        public CustomerBasedTaxFactory(Customer customer)
        {
            _customer = customer;
        }

        public class CustomerBasedTaxFactory : ITaxFactory
        {
            Customer _customer;
            static Dictionary<string, ITax> stateTaxObjects = new Dictionary<string, ITax>();
            static Dictionary<string, ITax> countyTaxObjects = new Dictionary<string, ITax>();
            public CustomerBasedTaxFactory(Customer customer)
            {
                _customer = customer;
            }
            public ITax GetTaxObject()
            {
                ITax tax;

                if (!string.IsNullOrEmpty(_customer.County))
                    if (!countyTaxObjects.Keys.Contains(_customer.StateCode))
                    {
                        tax = (ITax)Activator.CreateInstance("Tax", "solid.taxes." + _customer.County + "CountyTax");
                        countyTaxObjects.Add(_customer.StateCode, tax);
                    }
                    else
                        tax = countyTaxObjects[_customer.StateCode];
                else
                {
                    if (!stateTaxObjects.Keys.Contains(_customer.StateCode))
                    {
                        tax = (ITax)Activator.CreateInstance("Tax", "solid.taxes." + _customer.StateCode + "Tax");
                        stateTaxObjects.Add(_customer.StateCode, tax);
                    }
                    else
                        tax = stateTaxObjects[_customer.StateCode];
                }
                return tax;
            }
        }
    }
}
