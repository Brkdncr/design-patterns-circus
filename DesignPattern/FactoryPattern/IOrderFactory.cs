﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryPattern;

namespace DesignPattern.FactoryPattern
{
    public interface IOrderFactory
    {
        Order GetOrder(int orderId);
    }
}
