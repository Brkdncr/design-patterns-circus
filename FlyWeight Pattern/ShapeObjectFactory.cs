﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyWeight_Pattern
{
    public class ShapeObjectFactory
    {
        Dictionary<string, IShape> shapes = new Dictionary<string, IShape>();

        public int TotalObjectsCreated
        {
            get { return shapes.Count; }
        }

        public IShape GetShape(string ShapeName_)
        {
            IShape shape_ = null;

            if (shapes.ContainsKey(ShapeName_))
            {
                shape_ = shapes[ShapeName_];
            }
            else
            {
                switch (ShapeName_)
                {
                    case "Rectangle":
                        shape_ = new Rectangle();
                        shapes.Add(ShapeName_, shape_);
                        break;
                    case "Circle":
                        shape_ = new Circle();
                        shapes.Add(ShapeName_, shape_);
                        break;
                    default:
                        throw new Exception("Shape class not found ? ");
                }
            }
            return shape_;

        }


    }
}
