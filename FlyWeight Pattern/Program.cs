﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyWeight_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            ShapeObjectFactory sof_ = new ShapeObjectFactory();
            IShape shape_ = sof_.GetShape("Rectangle");
            shape_.Print();

            shape_ = sof_.GetShape("Circle");
            shape_.Print();

            shape_ = sof_.GetShape("Circle");
            shape_.Print();

            Console.WriteLine("Count of Objects : {0}", sof_.TotalObjectsCreated);


        }
    }
}
