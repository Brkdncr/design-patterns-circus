﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyWeight_Pattern
{
    public class Circle : IShape
    {
        public void Print()
        {
            Console.WriteLine("Circle printing.");
        }
    }
}
