﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyWeight_Pattern
{
    class Rectangle : IShape
    {
        public void Print()
        {
            Console.WriteLine("Rectangle drawing.");
        }
    }
}
