﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern3
{
    public class Document
    {
        private List<DocumentPart> m_parts;
        public string ToHTML()
        {
            string output = "";
            foreach (DocumentPart part in this.m_parts)
            {
                output += part.ToHtml();
            }
            return output;
        }
    }
}
