﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern3
{
    public class PlainText : DocumentPart
    {
        public override string ToHtml()
        {
            return this.Text;
        }
    }
}
