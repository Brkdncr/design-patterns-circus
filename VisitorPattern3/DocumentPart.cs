﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern3
{
    public abstract class DocumentPart
    {
        public string Text { get; private set; }
        public abstract string ToHtml();
    }

}
