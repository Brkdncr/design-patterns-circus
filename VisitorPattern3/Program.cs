﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern3
{
    class Program
    {
        enum DataType
        {
            Low,
            Medium,
            High
        }
        static void Main(string[] args)
        {
            DocumentPart boldText_ = new BoldText();
            boldText_.ToHtml();
        }
    }
}
