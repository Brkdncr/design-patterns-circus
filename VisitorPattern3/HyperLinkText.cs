﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern3
{
    public class HyperLinkText : DocumentPart
    {
        public string Url { get; private set; }

        public override string ToHtml()
        {
            return "<a href=\"" + this.Url + "\">" + this.Text + "</a>";
        }
    }
}
