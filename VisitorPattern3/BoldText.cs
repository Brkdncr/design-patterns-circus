﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern3
{
    public class BoldText : DocumentPart
    {
        public override string ToHtml()
        {
            return "<b>" + this.Text + "</b>";
        }
    }
}
