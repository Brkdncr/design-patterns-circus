﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern4
{
    public abstract class Animal
    {
        public string Head { get; set; }
        public string Body { get; set; }
        public string Leg { get; set; }
        public string Arm { get; set; }
        public string Tail { get; set; }

        public abstract void Eat();

    }
}
