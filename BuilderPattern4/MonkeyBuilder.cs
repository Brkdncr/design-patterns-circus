﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern4
{
    public class MonkeyBuilder : AnimalBuilder
    {
        public MonkeyBuilder()
        {
            aAnimal = new Monkey();
        }
        public override void BuildAnimalHeader()
        {
            aAnimal.Head = "";
        }

        public override void BuildAnimalBody()
        {
            aAnimal.Body = "";
        }

        public override void BuildAnimalLeg()
        {
            aAnimal.Leg = "";
        }

        public override void BuildAnimalArm()
        {
            aAnimal.Arm = "";
        }

        public override void BuildAnimalTail()
        {
            aAnimal.Tail = "";
        }
    }
}
