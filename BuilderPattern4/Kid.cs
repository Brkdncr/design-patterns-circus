﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern4
{
    public class Kid
    {
        public string Name { get; set; }

        public void MakeAnimal(AnimalBuilder aAnimalBuilder_)
        {
            aAnimalBuilder_.BuildAnimalHeader();
            aAnimalBuilder_.BuildAnimalBody();
            aAnimalBuilder_.BuildAnimalLeg();
            aAnimalBuilder_.BuildAnimalArm();
            aAnimalBuilder_.BuildAnimalTail();
        }
    }
}
