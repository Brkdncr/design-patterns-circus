﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern2
{
    public class Book : LibraryItem
    {
        private string _author;
        private string _title;

        public Book(string Author_,string Title_,int NumCopies_)
        {
            _author = Author_;
            _title = Title_;
            base.NumCopies = NumCopies_;
        }
        public override void Display()
        {
            Console.WriteLine("\nBook ------ ");
            Console.WriteLine(" Author: {0}", _author);
            Console.WriteLine(" Title: {0}", _title);
            Console.WriteLine(" # Copies: {0}", base.NumCopies);
        }
    }
}
