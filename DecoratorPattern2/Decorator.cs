﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern2
{
    public abstract class Decorator : LibraryItem
    {
        protected LibraryItem _libraryItem;

        public Decorator(LibraryItem LibraryItem_)
        {
            this._libraryItem = LibraryItem_;
        }
        public override void Display()
        {
            _libraryItem.Display();
        }
    }
}
