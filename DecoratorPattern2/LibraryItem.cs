﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern2
{
    public abstract class LibraryItem
    {
        private int _NumCopies;

        public int NumCopies
        {
            get { return _NumCopies; }
            set { _NumCopies = value; }
        }

        public abstract void Display();
    }
}
