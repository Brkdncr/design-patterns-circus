﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern2
{
    public class Borrowable : Decorator
    {
        protected List<string> _borrowers = new List<string>();

        public Borrowable(LibraryItem libraryItem)
            : base(libraryItem)
        {
        }

        public void BorrowItem(string Name_)
        {
            _borrowers.Add(Name_);
            _libraryItem.NumCopies--;

        }
        public void ReturnItem(string Name_)
        {
            _borrowers.Add(Name_);
            _libraryItem.NumCopies++;
        }

        public override void Display()
        {
            base.Display();

            foreach (string borrower in _borrowers)
            {
                Console.WriteLine(" borrower: " + borrower);
            }
        }

    }
}
