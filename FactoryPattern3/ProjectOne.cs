﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern3
{
    class ProjectOne : IBaseFactory
    {
        private SkillsSet _SkillTypeMobile;
        private SkillsSetWindows _SkillTypeDotNet;

        public ProjectOne(SkillsSetWindows SkillTypeDotNet_, SkillsSet SkillTypeMobile_)
        {
            this._SkillTypeDotNet = SkillTypeDotNet_;
            this._SkillTypeMobile = SkillTypeMobile_;
        }

        public IDotNetTeamFactory GetDotNetTeam()
        {
            switch (_SkillTypeDotNet)
            {
                case SkillsSetWindows.SilverLigt:
                    return new SilverLightTeam();
                    break;
                case SkillsSetWindows.Dotnet:
                    return new DotNetTeam();
                    break;
                default:
                    break;
            }
            return null;
        }

        public IMobileTechnologiesTeamFactory GetMobileTeam()
        {
            switch (_SkillTypeMobile)
            {
                case SkillsSet.Windows:
                    return new WindowsTeam();
                    break;
                case SkillsSet.Mobile:
                    return new AndroidTeam();
                    break;
                default:
                    break;
            }
            return null;
        }
    }
}
