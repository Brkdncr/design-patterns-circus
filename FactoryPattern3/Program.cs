﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern3
{
    class Program
    {
        static void Main(string[] args)
        {
            IDotNetTeamFactory getTeam_ = new GetDotNetTeam().GetTeam(SkillsSetWindows.Dotnet);
            Console.WriteLine(getTeam_.GetStream());
            IDotNetTeamFactory getTeamS_ = new GetDotNetTeam().GetTeam(SkillsSetWindows.SilverLigt);
            Console.WriteLine(getTeamS_.GetStream());
        }
    }
}
