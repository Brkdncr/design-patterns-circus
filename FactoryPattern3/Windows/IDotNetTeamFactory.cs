﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern3
{
    public interface IDotNetTeamFactory
    {
        string GetStream();
    }
}
