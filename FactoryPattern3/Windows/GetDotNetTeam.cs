﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern3
{
    public enum SkillsSetWindows
    {
        SilverLigt,
        Dotnet
    }
    public class GetDotNetTeam
    {
        public IDotNetTeamFactory GetTeam(SkillsSetWindows SkillType_)
        {
            switch (SkillType_)
            {
                case SkillsSetWindows.SilverLigt:
                    return new SilverLightTeam();
                    break;
                case SkillsSetWindows.Dotnet:
                    return new DotNetTeam();
                    break;
                default:
                    break;
            }
            return null;
        }
    }
}
