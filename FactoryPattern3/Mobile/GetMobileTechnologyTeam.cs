﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern3
{
    public enum SkillsSet
    {
        Windows,
        Mobile
    }
    public class GetMobileTechnologyTeam
    {
        public IMobileTechnologiesTeamFactory GetTeam(SkillsSet SkillType_)
        {
            switch (SkillType_)
            {
                case SkillsSet.Windows:
                    return new WindowsTeam();
                    break;
                case SkillsSet.Mobile:
                    return new AndroidTeam();
                    break;
                default:
                    break;
            }
            return null;
        }
    }
}
