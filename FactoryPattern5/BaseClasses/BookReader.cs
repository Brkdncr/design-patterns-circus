﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern5
{
    public abstract class BookReader
    {
        public Book Book { get; set; }

        public BookReader()
        {
            this.Book = BuyBook();
        }

        public abstract Book BuyBook();

        public Book BuyBook<T>()
            where T : Book, new()
        {
            return new T();
        }
        public void DisplayOwnedBooks()
        {
            Console.WriteLine(Book.GetType().ToString());
        }

    }
}
