﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern5
{
    class Program
    {
        static void Main(string[] args)
        {
            HorrorBookReader horrorBook_ = new HorrorBookReader();
            Book lordOfRings_ = horrorBook_.BuyBook<LordOfTheRings>();

            
        }
    }
}
