﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern2
{
    class Program
    {
        static void Main(string[] args)
        {
            TestWithStatePattern();
        }

        private static void TestWithStatePattern()
        {
            ATM atm = new ATM();

            atm.StartTheATM();
        }

        private static void TestWithoutStatePattern()
        {
            NoStateATM atm = new NoStateATM();

            while (true)
            {
                Console.WriteLine(atm.GetNextScreen());
            }
        }

    }
}
