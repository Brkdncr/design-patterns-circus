﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern2
{
    class NoCashState : ATMState
    {
        // This constructor will create new state taking values from old state
        public NoCashState(ATMState state)
            : this(state.DummyCashPresent, state.Atm)
        {

        }

        // this constructor will be used by the other one
        public NoCashState(int amountRemaining, ATM atmBeingUsed)
        {
            this.Atm = atmBeingUsed;
            this.DummyCashPresent = amountRemaining;
        }

        public override string GetNextScreen()
        {
            Console.WriteLine("ATM is EMPTY");
            Console.ReadLine();
            return string.Empty;
        }

        private void UpdateState()
        {
            if (this.DummyCashPresent == 0)
            {
                Atm.currentState = new NoCashState(this);
            }
            else
            {
                Atm.currentState = new CashWithdrawnState(this);
            }
        }
    }
}
