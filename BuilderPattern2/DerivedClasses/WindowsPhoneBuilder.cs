﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern2
{
  public  class WindowsPhoneBuilder : IPhoneBuilder
    {
      MobilePhone phone;

      public WindowsPhoneBuilder()
      {
          phone = new MobilePhone("Windows Phone Builder");
      }
      public void BuildScreen()
      {
          phone.PhoneScreen = ScreenType.ScreenType_TOUCH_CAPACITIVE;
      }

      public void BuildBattery()
      {
          phone.PhoneBattery = Battery.MAH_2000;
      }

      public void BuildOS()
      {
          phone.PhoneOS = OperatingSystem.WINDOWS_PHONE;
      }

      public void BuildStylus()
      {
          phone.PhoneStylus = Stylus.NO;
      }

      public MobilePhone Phone
      {
          get { return phone; }
      }
    }
}
