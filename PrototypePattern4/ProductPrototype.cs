﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern4
{
    public abstract class ProductPrototype
    {
        public decimal Price { get; set; }

        public ProductPrototype(decimal Price_)
        {
            this.Price = Price_;
        }

        public abstract ProductPrototype Clone();
    }
}
