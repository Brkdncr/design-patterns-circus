﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern4
{
    public class Milk : ProductPrototype
    {
        public Milk(decimal Price_)
            : base(Price_)
        {

        }
        public override ProductPrototype Clone()
        {
            return (Milk)this.MemberwiseClone();
        }
    }
}
