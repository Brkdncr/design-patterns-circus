﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern4
{
    public class CloneableProduct
    {
        public decimal Price { get; set; }

        public CloneableProduct(decimal price)
        {
            Price = price;
        }

        public object Clone()
        {
            // Shallow Copy: only top-level objects are duplicated
            return this.MemberwiseClone();

            // Deep Copy: all objects are duplicated
            //return this.Clone();
        }
    }
}
