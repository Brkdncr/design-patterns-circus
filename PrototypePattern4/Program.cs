﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern4
{
    class Program
    {
        static void Main(string[] args)
        {
            Bread bread1_ = new Bread(300);
            Bread bread2_ = new Bread(500);
            bread2_ = (Bread)bread1_;
        }
    }
}
