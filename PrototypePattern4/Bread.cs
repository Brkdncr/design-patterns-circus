﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern4
{
    public class Bread : ProductPrototype
    {
        public Bread(decimal Price_)
            : base(Price_)
        {

        }
        public override ProductPrototype Clone()
        {
            return (Bread)this.MemberwiseClone();
        }
    }
}
