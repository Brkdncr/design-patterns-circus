﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern
{
    class IPad : Tablet
    {
        public IPad(string model, string brand)
            : base(model, brand)
        {

        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);  
        }
    }
}
