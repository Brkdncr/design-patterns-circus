﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPattern
{
    public abstract class Tablet
    {
        public string Model { get; set; }
        public string Brand { get; set; }

        public Tablet(string model_,string brand_)
        {
            Model = model_;
            Brand = brand_;
        }

        public abstract void Accept(IVisitor visitor);
    }
}
